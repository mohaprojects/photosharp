INSERT INTO "Comments" ("CommentId","UserFK","ProductFK","CommentBody") VALUES (1,'a449e7a3-7c60-4616-8c6b-257a0c99dd8d',11,'This is the first comment comment for Nikon Z7II'),
 (2,'971a87c5-dd98-4308-9ac1-cde9f2757ba3',1,'This is the first comment for ARRI 1000'),
 (3,'c239e7ae-9217-477a-af65-38a317ec8a6e',21,'This is a comment for for Shape-GH5.'),
 (4,'d390add3-1a05-4ddb-99e0-8bbf3cbcb854',31,'This is a comment for Joby Telepod-325'),
 (5,'d390add3-1a05-4ddb-99e0-8bbf3cbcb854',11,'This is the second comment comment for Nikon Z7II'),
 (6,'d390add3-1a05-4ddb-99e0-8bbf3cbcb854',15,'This is the first comment for Canon EOS R6.'),
 (7,'c239e7ae-9217-477a-af65-38a317ec8a6e',15,'This is the second comment for Canon EOS R6.'),
 (8,'a449e7a3-7c60-4616-8c6b-257a0c99dd8d',15,'This is the third comment for Canon EOS R6.'),
 (9,'971a87c5-dd98-4308-9ac1-cde9f2757ba3',15,'This is the fourth comment for Canon EOS R6.'),
 (10,'c239e7ae-9217-477a-af65-38a317ec8a6e',17,'This is the first comment for Olympus Tough'),
 (11,'a449e7a3-7c60-4616-8c6b-257a0c99dd8d',32,'Comment as anonymous');

COMMIT;
