INSERT INTO "AspNetRoles" ("Id","Name","NormalizedName","ConcurrencyStamp") VALUES ('be130bb0-d466-44bb-9d3b-a829b2c28f78','User','USER','0f19a927-268b-4cf2-8e0c-cddd5a5b8660'),
 ('514122fc-76ab-4c61-88e0-37fb1a647fe5','Admin','ADMIN','592d3dc5-9b21-42b6-afdb-ba12cdf4cc89');
INSERT INTO "AspNetUsers" ("Id","FullName","Address","PostalCode","UserName","NormalizedUserName","Email","NormalizedEmail","EmailConfirmed","PasswordHash","SecurityStamp","ConcurrencyStamp","PhoneNumber","PhoneNumberConfirmed","TwoFactorEnabled","LockoutEnd","LockoutEnabled","AccessFailedCount") VALUES ('a449e7a3-7c60-4616-8c6b-257a0c99dd8d','Moha','White street','H6H 6Z6','marzhang@gmail.com','MARZHANG@GMAIL.COM','marzhang@gmail.com','MARZHANG@GMAIL.COM',1,'AQAAAAEAACcQAAAAEJ/gOJ9qnCa0S7hFDByu7bKvKz/VAg8UoNZ1uGIg5TnruWYPUioq/or+AB3m3sN4+Q==','VZGEHTFNHNELMXWGJ4KVDC6O2OTMNYR2','91a5c3a3-559e-441b-a6b5-898ece023a2a',NULL,0,0,NULL,1,0),
 ('971a87c5-dd98-4308-9ac1-cde9f2757ba3','Mona','Main Street','H5H 5M6','m@gmail.com','M@GMAIL.COM','m@gmail.com','M@GMAIL.COM',1,'AQAAAAEAACcQAAAAEMKVu1l2jOSWgI5/CjxsF3/2ZRQNXVaFaGm/oY75ZKUpIt55Xd5ksdJmcT3qVlIC5w==','XBUIQECXNQXTIPMIVSGNQTLQX27FUK57','40594bd4-0858-4cdb-9113-8456adb1c75d',NULL,0,0,NULL,1,0),
 ('c239e7ae-9217-477a-af65-38a317ec8a6e','Ziba','Blue street','M8M 2M7','z@mail.com','Z@MAIL.COM','z@mail.com','Z@MAIL.COM',1,'AQAAAAEAACcQAAAAEHCtay9f4JWVvI1BMrCwZx+/0xiucntYl+eGYumoJxV4o1ZLeelCQ/XIJiPeJ86pOw==','BFZRINJZYOK7T2Q4AUUEATVMWDSCZCPL','07ff58da-0d5f-465f-a6c4-ef8dd50e5e57',NULL,0,0,NULL,1,0),
 ('d390add3-1a05-4ddb-99e0-8bbf3cbcb854','Fred Zinnemann','Zen Street','H7U 8N8','f@mail.com','F@MAIL.COM','f@mail.com','F@MAIL.COM',1,'AQAAAAEAACcQAAAAEPyTwevXrqXOPOA25DK93o8g1+HK9iAfHed5VFtd/wviROFAqnCefGEcLv+IF3RPfw==','7QBI4PJA66RN2ZHPJNDBNKYTUWQULK7X','f7bb7d54-69ac-4fec-9bff-8f4f0ea4048e',NULL,0,0,NULL,1,0),
 ('cc0f83fd-50b9-4380-95f8-b146ea4f937d','Mina','Main Street','H5H 5M6','m@gmail.com',NULL,'m@gmail.com',NULL,1,NULL,'782462a9-e544-477b-8e0a-06e2cd0bfed6','826e0e8c-7c49-4e09-b935-ff4670db13b3',NULL,0,0,NULL,0,0),
 ('daacd938-504f-40c8-b790-0942adf1f398','Manaa','Main Street','H5H 5M6','m@gmail.com',NULL,'m@gmail.com',NULL,1,NULL,'e668c03d-cac6-4034-9fa7-4ad88492c798','7bd6f666-fe95-4073-94f5-6e0a05b8941a',NULL,0,0,NULL,0,0),
 ('6321ca0b-c183-4dd4-a2cd-60f443cfeecd',NULL,NULL,NULL,'admin@admin.com','ADMIN@ADMIN.COM','admin@admin.com','ADMIN@ADMIN.COM',1,'AQAAAAEAACcQAAAAEM73nXEHgu2ixLWjCF0d3WKNKUSTL80yBNbeLiIKwYiU4hEz+gKBRNsIBfI90PHS8w==','HTW2QBQS3PKG265UZXRE26JGUAGKGR62','7c19e167-673c-4070-8fca-94974cb757d7',NULL,0,0,NULL,1,0),
 ('da51f121-33c0-4bc6-bbb3-bd3982663446','Abdolla','1212, Street','M3H 2M5','ab@gmail.com','AB@GMAIL.COM','ab@gmail.com','AB@GMAIL.COM',1,'AQAAAAEAACcQAAAAEPY642oyOn3cexA4a9vV4sOTPR9mrJl4lnpPfHMFy2+EJLGEPt6OOh4ERPtUiMf0eg==','ODQOHLCVMJSZ3X6KWHP6O7MKGRFHWKQ7','b8ffa86e-707c-4789-8118-6bd9281e1ade',NULL,0,0,NULL,1,0);
INSERT INTO "Categories" ("CategoryId","CategoryName") VALUES (1,'Camera'),
 (2,'Shoulder'),
 (3,'Tripod'),
 (4,'Light');
INSERT INTO "AspNetUserRoles" ("UserId","RoleId") VALUES ('6321ca0b-c183-4dd4-a2cd-60f443cfeecd','514122fc-76ab-4c61-88e0-37fb1a647fe5'),
 ('da51f121-33c0-4bc6-bbb3-bd3982663446','be130bb0-d466-44bb-9d3b-a829b2c28f78');
INSERT INTO "Products" ("Id","ProductName","CategoryFK","ProductDescription","ProductImage1","ProductImage2","ProductImage3","Price","AvailableQuantity") VALUES (1,'ARRI 1000W',4,'* Brand: ARRI
* Weight: 20 kg
* Dimension: 80cm x 90cm x 40cm
* Type: Fresnel
* Bim Angle: 110 degrees
* Filter Mount: Included
* Lamp Type: LED
* Shader: Included','images/Light/ARRI 1000W.jpg','images/Light/ARRI 1000W.jpg','images/Light/ARRI 1000W.jpg','940.5',10),
 (2,'Godox Ad-s65s',4,'* Brand: Godox
* Weight: 20 kg
* Dimension: 80cm x 90cm x 40cm
* Type: Fresnel
* Bim Angle: 110 degrees
* Filter Mount: Included
* Lamp Type: LED
* Shader: Included','images/Light/Godox Ad-s65s.jpg','images/Light/Godox Ad-s65s.jpg','images/Light/Godox Ad-s65s.jpg','750.5',8),
 (3,'Godox SL-60',4,'* Brand: Godox
* Weight: 20 kg
* Dimension: 80cm x 90cm x 40cm
* Type: Fresnel
* Bim Angle: 110 degrees
* Filter Mount: Included
* Lamp Type: LED
* Shader: Included','images/Light/Godox SL-60.jpg','images/Light/Godox SL-60.jpg','images/Light/Godox SL-60.jpg','550.5',7),
 (4,'Impact One-Monolight',4,'* Brand: Impact
* Weight: 20 kg
* Dimension: 80cm x 90cm x 40cm
* Type: Fresnel
* Bim Angle: 110 degrees
* Filter Mount: Included
* Lamp Type: LED
* Shader: Included','images/Light/Impact One-Monolight.jpg','images/Light/Impact One-Monolight.jpg','images/Light/Impact One-Monolight.jpg','340.5',12),
 (5,'Impact Soft',4,'* Brand: Impact
* Weight: 20 kg
* Dimension: 80cm x 90cm x 40cm
* Type: Fresnel
* Bim Angle: 110 degrees
* Filter Mount: Included
* Lamp Type: LED
* Shader: Included','images/Light/Impact Soft.jpg','images/Light/Impact Soft.jpg','images/Light/Impact Soft.jpg','230.5',12),
 (6,'LUME CUBE',4,'* Brand: Lume Cube
* Weight: 20 kg
* Dimension: 80cm x 90cm x 40cm
* Type: Fresnel
* Bim Angle: 110 degrees
* Filter Mount: Included
* Lamp Type: LED
* Shader: Included','images/Light/LUME CUBE.jpg','images/Light/LUME CUBE.jpg','images/Light/LUME CUBE.jpg','130.5',4),
 (7,'MOUNTDOG 1350W',4,'* Brand: Mountdog
* Weight: 20 kg
* Dimension: 80cm x 90cm x 40cm
* Type: Fresnel
* Bim Angle: 110 degrees
* Filter Mount: Included
* Lamp Type: LED
* Shader: Included','images/Light/MOUNTDOG 1350W.jpg','images/Light/MOUNTDOG 1350W.jpg','images/Light/MOUNTDOG 1350W.jpg','130.5',4),
 (8,'White Umbrella',4,'* Brand: Umbrela
* Weight: 20 kg
* Dimension: 80cm x 90cm x 40cm
* Type: Fresnel
* Bim Angle: 110 degrees
* Filter Mount: Included
* Lamp Type: LED
* Shader: Included','images/Light/White Umbrella.jpg','images/Light/White Umbrella.jpg','images/Light/White Umbrella.jpg','4700.6',9),
 (9,'FS-3000',4,'* Brand: ARRI
* Weight: 20 kg
* Dimension: 80cm x 90cm x 40cm
* Type: Fresnel
* Bim Angle: 110 degrees
* Filter Mount: Included
* Lamp Type: LED
* Shader: Included','images/Light/FS-3000.jpg','images/Light/FS-3000.jpg','images/Light/FS-3000.jpg','700.6',19),
 (10,'Zimtown Softbox',4,'* Brand: Zimtown
* Weight: 20 kg
* Dimension: 80cm x 90cm x 40cm
* Type: Fresnel
* Bim Angle: 110 degrees
* Filter Mount: Included
* Lamp Type: LED
* Shader: Included','images/Light/Zimtown Softbox.jpg','images/Light/Zimtown Softbox.jpg','images/Light/Zimtown Softbox.jpg','6700.4',6),
 (11,'Nikon-Z7II',1,'* Brand: Nikon
* Weight: 1 kg
* Dimension: 20cm x 15cm x 7cm
* Lens Mount: EF
* CCD Type: CMOS
* Card Type: SD
* Internal Flash: Not included
* Photo size: 57 Megapixels','images/Camera/Nikon-Z7II.jpg','images/Camera/Nikon-Z7II-A.jpg','images/Camera/Nikon-Z7II-B.jpg','2450.4',8),
 (12,'Leica E67',1,'* Brand: Leica
* Weight: 1 kg
* Dimension: 20cm x 15cm x 7cm
* Lens Mount: EF
* CCD Type: CMOS
* Card Type: SD
* Internal Flash: Not included
* Photo size: 57 Megapixels','images/Camera/LeicaE67.jpg','images/Camera/LeicaE67-A.jpg','images/Camera/LeicaE67-B.jpg','2850.5',11),
 (13,'Canon 90D',1,'* Brand: Canon
* Weight: 1 kg
* Dimension: 20cm x 15cm x 7cm
* Lens Mount: EF
* CCD Type: CMOS
* Card Type: SD
* Internal Flash: Not included
* Photo size: 57 Megapixels','images/Camera/Canon_90D.jpg','images/Camera/Canon_90D-A.jpg','images/Camera/Canon_90D-B.jpg','3940.5',10),
 (14,'Canon EOS R5',1,'* Brand: Canon
* Weight: 1 kg
* Dimension: 20cm x 15cm x 7cm
* Lens Mount: EF
* CCD Type: CMOS
* Card Type: SD
* Internal Flash: Not included
* Photo size: 57 Megapixels','images/Camera/Canon EOS R5.jpg','images/Camera/Canon EOS R5-A.jpg','images/Camera/Canon EOS R5-B.jpg','2940.5',10),
 (15,'Canon EOS R6',1,'* Brand: Canon
* Weight: 1 kg
* Dimension: 20cm x 15cm x 7cm
* Lens Mount: EF
* CCD Type: CMOS
* Card Type: SD
* Internal Flash: Not included
* Photo size: 57 Megapixels','images/Camera/Canon EOS R6.jpg','images/Camera/Canon EOS R6-A.jpg','images/Camera/Canon EOS R6-B.jpg','2840.5',9),
 (16,'Nikon D850',1,'* Brand: Nikon
* Weight: 1 kg
* Dimension: 20cm x 15cm x 7cm
* Lens Mount: EF
* CCD Type: CMOS
* Card Type: SD
* Internal Flash: Not included
* Photo size: 57 Megapixels','images/Camera/Nikon D850.jpg','images/Camera/Nikon D850-A.jpg','images/Camera/Nikon D850-B.jpg','2760.2',12),
 (17,'Olympus Tough',1,'* Brand: Olymous
* Weight: 1 kg
* Dimension: 20cm x 15cm x 7cm
* Lens Mount: EF
* CCD Type: CMOS
* Card Type: SD
* Internal Flash: Not included
* Photo size: 57 Megapixels','images/Camera/Olympus Tough.jpg','images/Camera/Olympus Tough-A.jpg','images/Camera/Olympus Tough-B.jpg','960.4',18),
 (18,'Sony Alpha a7',1,'* Brand: Sony
* Weight: 1 kg
* Dimension: 20cm x 15cm x 7cm
* Lens Mount: EF
* CCD Type: CMOS
* Card Type: SD
* Internal Flash: Not included
* Photo size: 57 Megapixels','images/Camera/Sony Alpha a7.jpg','images/Camera/Sony Alpha a7-A.jpg','images/Camera/Sony Alpha a7-B.jpg','2650',8),
 (19,'Sony Alpha a7S',1,'* Brand: Sony
* Weight: 1 kg
* Dimension: 20cm x 15cm x 7cm
* Lens Mount: EF
* CCD Type: CMOS
* Card Type: SD
* Internal Flash: Not included
* Photo size: 57 Megapixels','images/Camera/Sony Alpha a7S.jpg','images/Camera/Sony Alpha a7S-A.jpg','images/Camera/Sony Alpha a7S-B.jpg','2680',7),
 (20,'Sony Cyber-shot',1,'* Brand: Sony
* Weight: 20 kg
* Dimension: 20cm x 15cm x 7cm
* Lens Mount: EF
* CCD Type: CMOS
* Card Type: SD
* Internal Flash: Not included
* Photo size: 57 Megapixels','images/Camera/Sony Cyber-shot.jpg','images/Camera/Sony Cyber-shot-A.jpg','images/Camera/Sony Cyber-shot-B.jpg','780',7),
 (21,'Shape-GH5',2,'* Brand: Shape
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Shoulder Pad: Included
* Material: Aluminium
* Compatibility: Photo - Video
* Waterproof: Yes
* Steady: Yes','images/Shoulder/Shape-GH5.jpg','images/Shoulder/Shape-GH5.jpg','images/Shoulder/Shape-GH5.jpg','780',12),
 (22,'Camtree Hunt',2,'* Brand: Shape
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Shoulder Pad: Included
* Material: Aluminium
* Compatibility: Photo - Video
* Waterproof: Yes
* Steady: Yes','images/Shoulder/Camtree Hunt.jpg','images/Shoulder/Camtree Hunt.jpg','images/Shoulder/Camtree Hunt.jpg','680',6),
 (23,'CAMVATE Pro',2,'* Brand: Camvate
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Shoulder Pad: Included
* Material: Aluminium
* Compatibility: Photo - Video
* Waterproof: Yes
* Steady: Yes','images/Shoulder/CAMVATE Pro.jpg','images/Shoulder/CAMVATE Pro.jpg','images/Shoulder/CAMVATE Pro.jpg','380',5),
 (24,'Filmcity DSLR',2,'* Brand: Filmcity
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Shoulder Pad: Included
* Material: Aluminium
* Compatibility: Photo - Video
* Waterproof: Yes
* Steady: Yes','images/Shoulder/Filmcity DSLR.jpg','images/Shoulder/Filmcity DSLR.jpg','images/Shoulder/Filmcity DSLR.jpg','380',5),
 (25,'Filmcity FC-02',2,'* Brand: Filmcity
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Shoulder Pad: Included
* Material: Aluminium
* Compatibility: Photo - Video
* Waterproof: Yes
* Steady: Yes','images/Shoulder/Filmcity FC-02.jpg','images/Shoulder/Filmcity FC-02.jpg','images/Shoulder/Filmcity FC-02.jpg','330',5),
 (26,'Filmcity Power',2,'* Brand: Filmcity
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Shoulder Pad: Included
* Material: Aluminium
* Compatibility: Photo - Video
* Waterproof: Yes
* Steady: Yes','images/Shoulder/Filmcity Power.jpg','images/Shoulder/Filmcity Power.jpg','images/Shoulder/Filmcity Power.jpg','630',4),
 (27,'Neewer DSLR',2,'* Brand: Neewer
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Shoulder Pad: Included
* Material: Aluminium
* Compatibility: Photo - Video
* Waterproof: Yes
* Steady: Yes','images/Shoulder/Neewer DSLR.jpg','images/Shoulder/Neewer DSLR.jpg','images/Shoulder/Neewer DSLR.jpg','770',11),
 (28,'Proaim Discovery',2,'* Brand: Prolam
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Shoulder Pad: Included
* Material: Aluminium
* Compatibility: Photo - Video
* Waterproof: Yes
* Steady: Yes','images/Shoulder/Proaim Discovery.jpg','images/Shoulder/Proaim Discovery.jpg','images/Shoulder/Proaim Discovery.jpg','570',3),
 (29,'Proaim Dovetail',2,'* Brand: Prolam
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Shoulder Pad: Included
* Material: Aluminium
* Compatibility: Photo - Video
* Waterproof: Yes
* Steady: Yes','images/Shoulder/Proaim Dovetail.jpg','images/Shoulder/Proaim Dovetail.jpg','images/Shoulder/Proaim Dovetail.jpg','470',3),
 (30,'Proaim Muffle',2,'* Brand: Prolam
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Shoulder Pad: Included
* Material: Aluminium
* Compatibility: Photo - Video
* Waterproof: Yes
* Steady: Yes','images/Shoulder/Proaim Muffle.jpg','images/Shoulder/Proaim Muffle.jpg','images/Shoulder/Proaim Muffle.jpg','680',7),
 (31,'JOBY TelePod 325',3,'* Brand: Joby
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Headtype: Photo - Video
* Material: Aluminium
* Beam Angle: 120 degrees
* Waterproof: Yes
* Bubble Spirit: Yes','images/Tripod/_ JOBY TelePod 325.jpg','images/Tripod/_ JOBY TelePod 325.jpg','images/Tripod/_ JOBY TelePod 325.jpg','180',12),
 (32,'Bosch BT160',3,'* Brand: Bosch
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Headtype: Photo - Video
* Material: Aluminium
* Beam Angle: 120 degrees
* Waterproof: Yes
* Bubble Spirit: Yes','images/Tripod/Bosch BT160.jpg','images/Tripod/Bosch BT160.jpg','images/Tripod/Bosch BT160.jpg','280',12),
 (33,'Cam-Gear-MS',3,'* Brand: Cam-Gear
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Headtype: Photo - Video
* Material: Aluminium
* Beam Angle: 120 degrees
* Waterproof: Yes
* Bubble Spirit: Yes','images/Tripod/Cam-Gear-MS.jpg','images/Tripod/Cam-Gear-MS.jpg','images/Tripod/Cam-Gear-MS.jpg','460',8),
 (34,'K&F SA254M1',3,'* Brand: K&F
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Headtype: Photo - Video
* Material: Aluminium
* Beam Angle: 120 degrees
* Waterproof: Yes
* Bubble Spirit: Yes','images/Tripod/K&F SA254M1.jpg','images/Tripod/K&F SA254M1.jpg','images/Tripod/K&F SA254M1.jpg','260',10),
 (35,'Manfrotto Compact',3,'* Brand: Manfrotto
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Headtype: Photo - Video
* Material: Aluminium
* Beam Angle: 120 degrees
* Waterproof: Yes
* Bubble Spirit: Yes','images/Tripod/Manfrotto Compact.jpg','images/Tripod/Manfrotto Compact.jpg','images/Tripod/Manfrotto Compact.jpg','280',8),
 (36,'Manfrotto MK290',3,'* Brand: Manfrotto
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Headtype: Photo - Video
* Material: Aluminium
* Beam Angle: 120 degrees
* Waterproof: Yes
* Bubble Spirit: Yes','images/Tripod/Manfrotto MK290 (1).jpg','images/Tripod/Manfrotto MK290 (1).jpg','images/Tripod/Manfrotto MK290 (1).jpg','460',8),
 (37,'Manfrotto-526',3,'* Brand: Manfrotto
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Headtype: Photo - Video
* Material: Aluminium
* Beam Angle: 120 degrees
* Waterproof: Yes
* Bubble Spirit: Yes','images/Tripod/Manfrotto-526.jpg','images/Tripod/Manfrotto-526.jpg','images/Tripod/Manfrotto-526.jpg','1060',12),
 (38,'Polaroid 72',3,'* Brand: Polaroid
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Headtype: Photo - Video
* Material: Aluminium
* Beam Angle: 120 degrees
* Waterproof: Yes
* Bubble Spirit: Yes','images/Tripod/Polaroid 72.jpg','images/Tripod/Polaroid 72.jpg','images/Tripod/Polaroid 72.jpg','660',7),
 (39,'PrimeCables Portable',3,'* Brand: Prime Cable
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Headtype: Photo - Video
* Material: Aluminium
* Beam Angle: 120 degrees
* Waterproof: Yes
* Bubble Spirit: Yes','images/Tripod/PrimeCables Portable.jpg','images/Tripod/PrimeCables Portable.jpg','images/Tripod/PrimeCables Portable.jpg','230',8),
 (40,'Ultimaxx 50',3,'* Brand: Ultimax
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Headtype: Photo - Video
* Material: Aluminium
* Beam Angle: 120 degrees
* Waterproof: Yes
* Bubble Spirit: Yes','images/Tripod/Ultimaxx 50.jpg','images/Tripod/Ultimaxx 50.jpg','images/Tripod/Ultimaxx 50.jpg','160',11),
 (41,'Nikon D80',1,'* Brand: ARRI
* Weight: 20 kg
* Dimension: 80cm x 90cm x 40cm
* Type: Fresnel
* Bim Angle: 110 degrees
* Filter Mount: Included
* Lamp Type: LED
* Shader: Included','images/Canon EOS R5.jpg',NULL,NULL,'0.0',0);
INSERT INTO "Comments" ("CommentId","UserFK","ProductFK","CommentBody") VALUES (1,'a449e7a3-7c60-4616-8c6b-257a0c99dd8d',11,'This is the first comment comment for Nikon Z7II'),
 (2,'971a87c5-dd98-4308-9ac1-cde9f2757ba3',1,'This is the first comment for ARRI 1000'),
 (3,'c239e7ae-9217-477a-af65-38a317ec8a6e',21,'This is a comment for for Shape-GH5.'),
 (4,'d390add3-1a05-4ddb-99e0-8bbf3cbcb854',31,'This is a comment for Joby Telepod-325'),
 (5,'d390add3-1a05-4ddb-99e0-8bbf3cbcb854',11,'This is the second comment comment for Nikon Z7II'),
 (6,'d390add3-1a05-4ddb-99e0-8bbf3cbcb854',15,'This is the first comment for Canon EOS R6.'),
 (7,'c239e7ae-9217-477a-af65-38a317ec8a6e',15,'This is the second comment for Canon EOS R6.'),
 (8,'a449e7a3-7c60-4616-8c6b-257a0c99dd8d',15,'This is the third comment for Canon EOS R6.'),
 (9,'971a87c5-dd98-4308-9ac1-cde9f2757ba3',15,'This is the fourth comment for Canon EOS R6.'),
 (10,'c239e7ae-9217-477a-af65-38a317ec8a6e',17,'This is the first comment for Olympus Tough'),
 (11,'a449e7a3-7c60-4616-8c6b-257a0c99dd8d',32,'Comment as anonymous');
INSERT INTO "Orders" ("OrderId","ProductFK","UserFK","Quantity","OrderDate","Total") VALUES 
 (1,1,'a449e7a3-7c60-4616-8c6b-257a0c99dd8d',1,'2021-06-06 17:13:06.648807', 940.00),
 (2,11,'971a87c5-dd98-4308-9ac1-cde9f2757ba3',1,'2021-06-08 17:13:06.648807', 2450.40),
 (3,4,'d390add3-1a05-4ddb-99e0-8bbf3cbcb854',1,'2021-06-06 17:13:06.648807', 340.50),
 (4,21,'da51f121-33c0-4bc6-bbb3-bd3982663446',1,'2021-06-07 17:13:06.648807', 780.00),
 (5,15,'da51f121-33c0-4bc6-bbb3-bd3982663446',1,'2021-06-08 17:13:06.648807', 2840.50),
 (6,31,'da51f121-33c0-4bc6-bbb3-bd3982663446',1,'2021-06-09 17:13:06.648807', 180);

COMMIT;
