INSERT INTO "Products" ("Id","ProductName","CategoryFK","ProductDescription","ProductImage1","ProductImage2","ProductImage3","Price","AvailableQuantity") VALUES (1,'ARRI 1000W',4,'* Brand: ARRI
* Weight: 20 kg
* Dimension: 80cm x 90cm x 40cm
* Type: Fresnel
* Bim Angle: 110 degrees
* Filter Mount: Included
* Lamp Type: LED
* Shader: Included','images/Light/ARRI 1000W.jpg','images/Light/ARRI 1000W.jpg','images/Light/ARRI 1000W.jpg','940.5',10),
 (2,'Godox Ad-s65s',4,'* Brand: Godox
* Weight: 20 kg
* Dimension: 80cm x 90cm x 40cm
* Type: Fresnel
* Bim Angle: 110 degrees
* Filter Mount: Included
* Lamp Type: LED
* Shader: Included','images/Light/Godox Ad-s65s.jpg','images/Light/Godox Ad-s65s.jpg','images/Light/Godox Ad-s65s.jpg','750.5',8),
 (3,'Godox SL-60',4,'* Brand: Godox
* Weight: 20 kg
* Dimension: 80cm x 90cm x 40cm
* Type: Fresnel
* Bim Angle: 110 degrees
* Filter Mount: Included
* Lamp Type: LED
* Shader: Included','images/Light/Godox SL-60.jpg','images/Light/Godox SL-60.jpg','images/Light/Godox SL-60.jpg','550.5',15),
 (4,'Impact One-Monolight',4,'* Brand: Impact
* Weight: 20 kg
* Dimension: 80cm x 90cm x 40cm
* Type: Fresnel
* Bim Angle: 110 degrees
* Filter Mount: Included
* Lamp Type: LED
* Shader: Included','images/Light/Impact One-Monolight.jpg','images/Light/Impact One-Monolight.jpg','images/Light/Impact One-Monolight.jpg','340.5',12),
 (5,'Impact Soft',4,'* Brand: Impact
* Weight: 20 kg
* Dimension: 80cm x 90cm x 40cm
* Type: Fresnel
* Bim Angle: 110 degrees
* Filter Mount: Included
* Lamp Type: LED
* Shader: Included','images/Light/Impact Soft.jpg','images/Light/Impact Soft.jpg','images/Light/Impact Soft.jpg','230.5',12),
 (6,'LUME CUBE',4,'* Brand: Lume Cube
* Weight: 20 kg
* Dimension: 80cm x 90cm x 40cm
* Type: Fresnel
* Bim Angle: 110 degrees
* Filter Mount: Included
* Lamp Type: LED
* Shader: Included','images/Light/LUME CUBE.jpg','images/Light/LUME CUBE.jpg','images/Light/LUME CUBE.jpg','130.5',4),
 (7,'MOUNTDOG 1350W',4,'* Brand: Mountdog
* Weight: 20 kg
* Dimension: 80cm x 90cm x 40cm
* Type: Fresnel
* Bim Angle: 110 degrees
* Filter Mount: Included
* Lamp Type: LED
* Shader: Included','images/Light/MOUNTDOG 1350W.jpg','images/Light/MOUNTDOG 1350W.jpg','images/Light/MOUNTDOG 1350W.jpg','130.5',4),
 (8,'White Umbrella',4,'* Brand: Umbrela
* Weight: 20 kg
* Dimension: 80cm x 90cm x 40cm
* Type: Fresnel
* Bim Angle: 110 degrees
* Filter Mount: Included
* Lamp Type: LED
* Shader: Included','images/Light/White Umbrella.jpg','images/Light/White Umbrella.jpg','images/Light/White Umbrella.jpg','4700.6',9),
 (9,'FS-3000',4,'* Brand: ARRI
* Weight: 20 kg
* Dimension: 80cm x 90cm x 40cm
* Type: Fresnel
* Bim Angle: 110 degrees
* Filter Mount: Included
* Lamp Type: LED
* Shader: Included','images/Light/FS-3000.jpg','images/Light/FS-3000.jpg','images/Light/FS-3000.jpg','700.6',19),
 (10,'Zimtown Softbox',4,'* Brand: Zimtown
* Weight: 20 kg
* Dimension: 80cm x 90cm x 40cm
* Type: Fresnel
* Bim Angle: 110 degrees
* Filter Mount: Included
* Lamp Type: LED
* Shader: Included','images/Light/Zimtown Softbox.jpg','images/Light/Zimtown Softbox.jpg','images/Light/Zimtown Softbox.jpg','6700.4',6),
 (11,'Nikon-Z7II',1,'* Brand: Nikon
* Weight: 1 kg
* Dimension: 20cm x 15cm x 7cm
* Lens Mount: EF
* CCD Type: CMOS
* Card Type: SD
* Internal Flash: Not included
* Photo size: 57 Megapixels','images/Camera/Nikon-Z7II.jpg','images/Camera/Nikon-Z7II-A.jpg','images/Camera/Nikon-Z7II-B.jpg','2450.4',8),
 (12,'Leica E67',1,'* Brand: Leica
* Weight: 1 kg
* Dimension: 20cm x 15cm x 7cm
* Lens Mount: EF
* CCD Type: CMOS
* Card Type: SD
* Internal Flash: Not included
* Photo size: 57 Megapixels','images/Camera/LeicaE67.jpg','images/Camera/LeicaE67-A.jpg','images/Camera/LeicaE67-B.jpg','2850.5',11),
 (13,'Canon 90D',1,'* Brand: Canon
* Weight: 1 kg
* Dimension: 20cm x 15cm x 7cm
* Lens Mount: EF
* CCD Type: CMOS
* Card Type: SD
* Internal Flash: Not included
* Photo size: 57 Megapixels','images/Camera/Canon_90D.jpg','images/Camera/Canon_90D-A.jpg','images/Camera/Canon_90D-B.jpg','3940.5',10),
 (14,'Canon EOS R5',1,'* Brand: Canon
* Weight: 1 kg
* Dimension: 20cm x 15cm x 7cm
* Lens Mount: EF
* CCD Type: CMOS
* Card Type: SD
* Internal Flash: Not included
* Photo size: 57 Megapixels','images/Camera/Canon EOS R5.jpg','images/Camera/Canon EOS R5-A.jpg','images/Camera/Canon EOS R5-B.jpg','2940.5',10),
 (15,'Canon EOS R6',1,'* Brand: Canon
* Weight: 1 kg
* Dimension: 20cm x 15cm x 7cm
* Lens Mount: EF
* CCD Type: CMOS
* Card Type: SD
* Internal Flash: Not included
* Photo size: 57 Megapixels','images/Camera/Canon EOS R6.jpg','images/Camera/Canon EOS R6-A.jpg','images/Camera/Canon EOS R6-B.jpg','2840.5',9),
 (16,'Nikon D850',1,'* Brand: Nikon
* Weight: 1 kg
* Dimension: 20cm x 15cm x 7cm
* Lens Mount: EF
* CCD Type: CMOS
* Card Type: SD
* Internal Flash: Not included
* Photo size: 57 Megapixels','images/Camera/Nikon D850.jpg','images/Camera/Nikon D850-A.jpg','images/Camera/Nikon D850-B.jpg','2760.2',12),
 (17,'Olympus Tough',1,'* Brand: Olymous
* Weight: 1 kg
* Dimension: 20cm x 15cm x 7cm
* Lens Mount: EF
* CCD Type: CMOS
* Card Type: SD
* Internal Flash: Not included
* Photo size: 57 Megapixels','images/Camera/Olympus Tough.jpg','images/Camera/Olympus Tough-A.jpg','images/Camera/Olympus Tough-B.jpg','960.4',18),
 (18,'Sony Alpha a7',1,'* Brand: Sony
* Weight: 1 kg
* Dimension: 20cm x 15cm x 7cm
* Lens Mount: EF
* CCD Type: CMOS
* Card Type: SD
* Internal Flash: Not included
* Photo size: 57 Megapixels','images/Camera/Sony Alpha a7.jpg','images/Camera/Sony Alpha a7-A.jpg','images/Camera/Sony Alpha a7-B.jpg','2650',8),
 (19,'Sony Alpha a7S',1,'* Brand: Sony
* Weight: 1 kg
* Dimension: 20cm x 15cm x 7cm
* Lens Mount: EF
* CCD Type: CMOS
* Card Type: SD
* Internal Flash: Not included
* Photo size: 57 Megapixels','images/Camera/Sony Alpha a7S.jpg','images/Camera/Sony Alpha a7S-A.jpg','images/Camera/Sony Alpha a7S-B.jpg','2680',7),
 (20,'Sony Cyber-shot',1,'* Brand: Sony
* Weight: 20 kg
* Dimension: 20cm x 15cm x 7cm
* Lens Mount: EF
* CCD Type: CMOS
* Card Type: SD
* Internal Flash: Not included
* Photo size: 57 Megapixels','images/Camera/Sony Cyber-shot.jpg','images/Camera/Sony Cyber-shot-A.jpg','images/Camera/Sony Cyber-shot-B.jpg','780',7),
 (21,'Shape-GH5',2,'* Brand: Shape
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Shoulder Pad: Included
* Material: Aluminium
* Compatibility: Photo - Video
* Waterproof: Yes
* Steady: Yes','images/Shoulder/Shape-GH5.jpg','images/Shoulder/Shape-GH5.jpg','images/Shoulder/Shape-GH5.jpg','780',12),
 (22,'Camtree Hunt',2,'* Brand: Shape
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Shoulder Pad: Included
* Material: Aluminium
* Compatibility: Photo - Video
* Waterproof: Yes
* Steady: Yes','images/Shoulder/Camtree Hunt.jpg','images/Shoulder/Camtree Hunt.jpg','images/Shoulder/Camtree Hunt.jpg','680',6),
 (23,'CAMVATE Pro',2,'* Brand: Camvate
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Shoulder Pad: Included
* Material: Aluminium
* Compatibility: Photo - Video
* Waterproof: Yes
* Steady: Yes','images/Shoulder/CAMVATE Pro.jpg','images/Shoulder/CAMVATE Pro.jpg','images/Shoulder/CAMVATE Pro.jpg','380',5),
 (24,'Filmcity DSLR',2,'* Brand: Filmcity
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Shoulder Pad: Included
* Material: Aluminium
* Compatibility: Photo - Video
* Waterproof: Yes
* Steady: Yes','images/Shoulder/Filmcity DSLR.jpg','images/Shoulder/Filmcity DSLR.jpg','images/Shoulder/Filmcity DSLR.jpg','380',5),
 (25,'Filmcity FC-02',2,'* Brand: Filmcity
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Shoulder Pad: Included
* Material: Aluminium
* Compatibility: Photo - Video
* Waterproof: Yes
* Steady: Yes','images/Shoulder/Filmcity FC-02.jpg','images/Shoulder/Filmcity FC-02.jpg','images/Shoulder/Filmcity FC-02.jpg','330',5),
 (26,'Filmcity Power',2,'* Brand: Filmcity
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Shoulder Pad: Included
* Material: Aluminium
* Compatibility: Photo - Video
* Waterproof: Yes
* Steady: Yes','images/Shoulder/Filmcity Power.jpg','images/Shoulder/Filmcity Power.jpg','images/Shoulder/Filmcity Power.jpg','630',4),
 (27,'Neewer DSLR',2,'* Brand: Neewer
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Shoulder Pad: Included
* Material: Aluminium
* Compatibility: Photo - Video
* Waterproof: Yes
* Steady: Yes','images/Shoulder/Neewer DSLR.jpg','images/Shoulder/Neewer DSLR.jpg','images/Shoulder/Neewer DSLR.jpg','770',11),
 (28,'Proaim Discovery',2,'* Brand: Prolam
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Shoulder Pad: Included
* Material: Aluminium
* Compatibility: Photo - Video
* Waterproof: Yes
* Steady: Yes','images/Shoulder/Proaim Discovery.jpg','images/Shoulder/Proaim Discovery.jpg','images/Shoulder/Proaim Discovery.jpg','570',3),
 (29,'Proaim Dovetail',2,'* Brand: Prolam
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Shoulder Pad: Included
* Material: Aluminium
* Compatibility: Photo - Video
* Waterproof: Yes
* Steady: Yes','images/Shoulder/Proaim Dovetail.jpg','images/Shoulder/Proaim Dovetail.jpg','images/Shoulder/Proaim Dovetail.jpg','470',3),
 (30,'Proaim Muffle',2,'* Brand: Prolam
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Shoulder Pad: Included
* Material: Aluminium
* Compatibility: Photo - Video
* Waterproof: Yes
* Steady: Yes','images/Shoulder/Proaim Muffle.jpg','images/Shoulder/Proaim Muffle.jpg','images/Shoulder/Proaim Muffle.jpg','680',7),
 (31,'JOBY TelePod 325',3,'* Brand: Joby
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Headtype: Photo - Video
* Material: Aluminium
* Beam Angle: 120 degrees
* Waterproof: Yes
* Bubble Spirit: Yes','images/Tripod/_ JOBY TelePod 325.jpg','images/Tripod/_ JOBY TelePod 325.jpg','images/Tripod/_ JOBY TelePod 325.jpg','180',12),
 (32,'Bosch BT160',3,'* Brand: Bosch
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Headtype: Photo - Video
* Material: Aluminium
* Beam Angle: 120 degrees
* Waterproof: Yes
* Bubble Spirit: Yes','images/Tripod/Bosch BT160.jpg','images/Tripod/Bosch BT160.jpg','images/Tripod/Bosch BT160.jpg','280',12),
 (33,'Cam-Gear-MS',3,'* Brand: Cam-Gear
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Headtype: Photo - Video
* Material: Aluminium
* Beam Angle: 120 degrees
* Waterproof: Yes
* Bubble Spirit: Yes','images/Tripod/Cam-Gear-MS.jpg','images/Tripod/Cam-Gear-MS.jpg','images/Tripod/Cam-Gear-MS.jpg','460',8),
 (34,'K&F SA254M1',3,'* Brand: K&F
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Headtype: Photo - Video
* Material: Aluminium
* Beam Angle: 120 degrees
* Waterproof: Yes
* Bubble Spirit: Yes','images/Tripod/K&F SA254M1.jpg','images/Tripod/K&F SA254M1.jpg','images/Tripod/K&F SA254M1.jpg','260',10),
 (35,'Manfrotto Compact',3,'* Brand: Manfrotto
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Headtype: Photo - Video
* Material: Aluminium
* Beam Angle: 120 degrees
* Waterproof: Yes
* Bubble Spirit: Yes','images/Tripod/Manfrotto Compact.jpg','images/Tripod/Manfrotto Compact.jpg','images/Tripod/Manfrotto Compact.jpg','280',8),
 (36,'Manfrotto MK290',3,'* Brand: Manfrotto
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Headtype: Photo - Video
* Material: Aluminium
* Beam Angle: 120 degrees
* Waterproof: Yes
* Bubble Spirit: Yes','images/Tripod/Manfrotto MK290 (1).jpg','images/Tripod/Manfrotto MK290 (1).jpg','images/Tripod/Manfrotto MK290 (1).jpg','460',8),
 (37,'Manfrotto-526',3,'* Brand: Manfrotto
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Headtype: Photo - Video
* Material: Aluminium
* Beam Angle: 120 degrees
* Waterproof: Yes
* Bubble Spirit: Yes','images/Tripod/Manfrotto-526.jpg','images/Tripod/Manfrotto-526.jpg','images/Tripod/Manfrotto-526.jpg','1060',12),
 (38,'Polaroid 72',3,'* Brand: Polaroid
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Headtype: Photo - Video
* Material: Aluminium
* Beam Angle: 120 degrees
* Waterproof: Yes
* Bubble Spirit: Yes','images/Tripod/Polaroid 72.jpg','images/Tripod/Polaroid 72.jpg','images/Tripod/Polaroid 72.jpg','660',7),
 (39,'PrimeCables Portable',3,'* Brand: Prime Cable
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Headtype: Photo - Video
* Material: Aluminium
* Beam Angle: 120 degrees
* Waterproof: Yes
* Bubble Spirit: Yes','images/Tripod/PrimeCables Portable.jpg','images/Tripod/PrimeCables Portable.jpg','images/Tripod/PrimeCables Portable.jpg','230',8),
 (40,'Ultimaxx 50',3,'* Brand: Ultimax
* Weight: 4 kg
* Dimension: 50cm x 35cm x 30cm
* Headtype: Photo - Video
* Material: Aluminium
* Beam Angle: 120 degrees
* Waterproof: Yes
* Bubble Spirit: Yes','images/Tripod/Ultimaxx 50.jpg','images/Tripod/Ultimaxx 50.jpg','images/Tripod/Ultimaxx 50.jpg','160',11);

COMMIT;
