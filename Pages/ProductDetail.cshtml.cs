using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PhotoSharp.Data;
using PhotoSharp.Models;


namespace PhotoSharp.Pages
{
    public class ProductDetailModel : PageModel
    {
        private PhotoSharpDbContext db;
        private readonly SignInManager<User> signInManager;
        public ProductDetailModel(PhotoSharpDbContext db, SignInManager<User> signInManager)
        {
            this.db = db;
            this.signInManager = signInManager;
        }
        [BindProperty(SupportsGet = true)]
        public int Id { get; set; }
        [BindProperty]
        public Product Product { get; set; }
        [BindProperty]
        public InputModel Input { get; set; }
        public class InputModel
        {
            public int CommentId { get; set; }
            public User User { get; set; }
            [ForeignKey("User")]
            public string UserFK { get; set; }
            public Product Product { get; set; }
            [ForeignKey("Product")]
            public int ProductFK { get; set; }
            [Required(ErrorMessage = "The comment could not be empty.")]
            public string CommentBody { get; set; }
        }
        public List<Comment> CommentList2 { get; set; } = new List<Comment>();
        [BindProperty]
        public List<Comment> CommentList { get; set; } = new List<Comment>();
        public Hashtable CommentHash = new Hashtable();
        public IActionResult OnGet(int? Id)
        {
            Product = db.Products.Find(Id);
            Product = db.Products.FirstOrDefault(m => m.Id == Id);
            if (Id == null || Product == null) 
            {
                return RedirectToPage("PageNotFound");
            }
            CommentList = db.Comments.ToList();
            foreach (var item in CommentList)
            {
                if (item.ProductFK == Id)
                {
                    var user = (from usr in db.Users
                                where usr.Id == item.UserFK
                                select usr).FirstOrDefault();
                    CommentHash.Add(item.CommentBody, user.FullName);
                }
            }
            var data2 = db.Comments.OrderByDescending(c => c.CommentId).Take(2).ToList();
            CommentList2 = data2;
            return Page();
        }
        public IActionResult OnPost()
        {
            Product = db.Products.Find(Id);
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (!signInManager.IsSignedIn(User))
            {
                return RedirectToPage("./Login");
            }
            if (ModelState.IsValid)
            {
                var NewComment = new Comment { UserFK = userId, ProductFK = Product.Id, CommentBody = Input.CommentBody };
                db.Comments.Add(NewComment);
                db.SaveChanges();
                return RedirectToPage("./CommentSuccess");
            }
            return Page();
        }
    }
}