using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using PhotoSharp.Data;
using PhotoSharp.Models;
using System.IO;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using System.Text.RegularExpressions;

using Microsoft.Extensions.Hosting;

namespace Photosharp.Pages
{

    [Authorize(Roles = "Admin")]
    public class AddProductModel : PageModel
    {
        private PhotoSharpDbContext db;
        private readonly ILogger<RegisterModel> logger;
        private IHostEnvironment _environment;
        public AddProductModel(PhotoSharpDbContext db, ILogger<RegisterModel> logger, IHostEnvironment environment)
        {
            this.db = db;
            this.logger = logger;
            _environment = environment;
        }
        [BindProperty]
        public Comment Comment { get; set; }
        [BindProperty]
        public List<Comment> CommentList2 { get; set; } = new List<Comment>();
        [BindProperty]
        public string ProductName { get; set; }
        public Category Category { get; set; }
        [ForeignKey("Category")]
        public int CategoryFK { get; set; }

        [BindProperty]
        public string ProductDescription { get; set; }
        public string ProductImage1 { get; set; }
        public string ProductImage2 { get; set; }
        public string ProductImage3 { get; set; }
        [BindProperty]
        public decimal Price { get; set; }
        [BindProperty]
        public int AvailableQuantity { get; set; }


        [BindProperty]
        public IFormFile Upload { get; set; }
        [BindProperty]
        public IFormFile UploadTwo { get; set; }
        [BindProperty]
        public IFormFile UploadThree { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid)
            {
// **************** IMAGE 1 **********************
                string imagePath = null;

                if (Upload != null)
                {
                    string fileExtension = Path.GetExtension(Upload.FileName).ToLower();
                    string[] allowedExtensions = { ".jpg", ".jpeg", ".gif", ".png" };
                    if (!allowedExtensions.Contains(fileExtension))
                    {
                        ModelState.AddModelError(string.Empty, "Only image files (jpg, jpeg, gif, png) are allowed");
                        return Page();
                    }
                    var invalids = System.IO.Path.GetInvalidFileNameChars();
                    var newFileName = String.Join("_", Upload.FileName.Split(invalids, StringSplitOptions.RemoveEmptyEntries)).TrimEnd('.');
                    var destPath = Path.Combine(_environment.ContentRootPath, "wwwroot", "images/", Upload.FileName);
                    try
                    {
                        using (var fileStream = new FileStream(destPath, FileMode.Create))
                        {
                            Upload.CopyTo(fileStream);
                        }
                    }
                    catch (Exception ex) when (ex is IOException || ex is SystemException)
                    {
                        ModelState.AddModelError(string.Empty, "Internal error saving the uploaded file");
                        return Page();
                    }
                    imagePath = Path.Combine("images/", newFileName);
// **************** IMAGE 2 **********************
                    string imagePathTwo = null;

                    if (UploadTwo != null)
                    {
                        string fileExtensionTwo = Path.GetExtension(UploadTwo.FileName).ToLower();
                        string[] allowedExtensionTwo = { ".jpg", ".jpeg", ".gif", ".png" };
                        if (!allowedExtensionTwo.Contains(fileExtensionTwo))
                        {
                            // Display error and the form again
                            ModelState.AddModelError(string.Empty, "Only image files (jpg, jpeg, gif, png) are allowed");
                            return Page();
                        }
                        // FIXME: sanitize the original name or assign random
                        var invalidsTwo = System.IO.Path.GetInvalidFileNameChars();
                        var newFileNameTwo = String.Join("_", UploadTwo.FileName.Split(invalidsTwo, StringSplitOptions.RemoveEmptyEntries)).TrimEnd('.');

                        var destPathTwo = Path.Combine(_environment.ContentRootPath, "wwwroot", "images/", UploadTwo.FileName);
                        // FIXME: handle IO errors when copying the file
                        try
                        {
                            using (var fileStreamTwo = new FileStream(destPathTwo, FileMode.Create))
                            {
                                UploadTwo.CopyTo(fileStreamTwo);
                            }
                        }
                        catch (Exception ex) when (ex is IOException || ex is SystemException)
                        {
                            // TODO: Log this as an error
                            ModelState.AddModelError(string.Empty, "Internal error saving the uploaded file");
                            return Page();
                        }

                        imagePathTwo = Path.Combine("images/", newFileNameTwo);
                    }
// ************** IMAGE  3 **********************
                    string imagePathThree = null;
                    if (UploadThree != null)
                    {
                        string fileExtensionThree = Path.GetExtension(UploadThree.FileName).ToLower();
                        string[] allowedExtensionThree = { ".jpg", ".jpeg", ".gif", ".png" };
                        if (!allowedExtensionThree.Contains(fileExtensionThree))
                        {
                            ModelState.AddModelError(string.Empty, "Only image files (jpg, jpeg, gif, png) are allowed");
                            return Page();
                        }
                        var invalidsThree = System.IO.Path.GetInvalidFileNameChars();
                        var newFileNameThree = String.Join("_", UploadThree.FileName.Split(invalidsThree, StringSplitOptions.RemoveEmptyEntries)).TrimEnd('.');
                        var destPathThree = Path.Combine(_environment.ContentRootPath, "wwwroot", "images/", UploadThree.FileName);
                        try
                        {
                            using (var fileStreamThree = new FileStream(destPathThree, FileMode.Create))
                            {
                                UploadThree.CopyTo(fileStreamThree);
                            }
                        }
                        catch (Exception ex) when (ex is IOException || ex is SystemException)
                        {
                            ModelState.AddModelError(string.Empty, "Internal error saving the uploaded file");
                            return Page();
                        }
                        imagePathThree = Path.Combine("images/", newFileNameThree);
                    }
                    var CategoryFK = Request.Form["CategoryFK"];
                    int CategoryFKInt = Convert.ToInt32(CategoryFK);
                    var newProduct = new PhotoSharp.Models.Product
                    {
                        ProductImage1 = imagePath,
                        ProductImage2 = imagePathTwo,
                        ProductImage3 = imagePathThree,
                        ProductName = ProductName,
                        ProductDescription = ProductDescription,
                        CategoryFK = CategoryFKInt,
                        Price = Price,
                        AvailableQuantity = AvailableQuantity
                    };
                    db.Add(newProduct);
                    await db.SaveChangesAsync();
                    return RedirectToPage("AddProductSuccess");
                }
            }
            return Page();
        }
        public void OnGet()
        {
            var data2 = db.Comments.OrderByDescending(c => c.CommentId).Take(2).ToList();
            CommentList2 = data2;
        }
    }
}
