using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PhotoSharp.Data;
using PhotoSharp.Models;

namespace Photosharp.Pages
{
    public class OrderSuccessModel : PageModel
    {
        private PhotoSharpDbContext db;  
        public OrderSuccessModel(PhotoSharpDbContext db) => this.db = db;
        
        [BindProperty]
        public Comment Comment { get; set; }
        public List<Comment> CommentList2 { get; set; } = new List<Comment>();
        public void OnGet()
        {
            var data2 = db.Comments.OrderByDescending(c => c.CommentId).Take(2).ToList();
                CommentList2 = data2;
        }
    }
}
