using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using PhotoSharp.Models;
using PhotoSharp.Data;
namespace Photosharp.Pages
{
    public class RegisterModel : PageModel
    {
        private PhotoSharpDbContext db;
        private readonly UserManager<User> userManager;
        private readonly ILogger<RegisterModel> logger;
        [BindProperty]
        public InputModel Input { get; set; }
        public RegisterModel(UserManager<User> userManager,
              ILogger<RegisterModel> logger, PhotoSharpDbContext db)
        {
            this.userManager = userManager;
            this.logger = logger;
            this.db = db;
        }
        [BindProperty]
        public Comment Comment { get; set; }
        [BindProperty]
        public List<Comment> CommentList2 { get; set; } = new List<Comment>();
        public class InputModel
        {
            [Required]
            [Display(Name = "FullName")]
            public string FullName { get; set; }
            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }
            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }
            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }
            public string Address { get; set; }
            public string PostalCode { get; set; }
        }
        public void OnGet()
        {
            var data2 = db.Comments.OrderByDescending(c => c.CommentId).Take(2).ToList();

            CommentList2 = data2;
        }
        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid)
            {
                var user = new User { UserName = Input.Email, Email = Input.Email, EmailConfirmed = true, FullName = Input.FullName, Address = Input.Address, PostalCode = Input.PostalCode, };
                var result = await userManager.CreateAsync(user, Input.Password);
                if (result.Succeeded)
                {
                    var result2 = await userManager.AddToRoleAsync(user, "User"); 
                        if (result2.Succeeded) { 
                    logger.LogInformation($"User {Input.Email} create a new account with password");
                    return RedirectToPage("RegisterSuccess", new { email = Input.Email });
                      } else { 
                    } 
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }
            return Page();
        }
    }
}
