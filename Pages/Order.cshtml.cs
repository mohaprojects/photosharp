using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PhotoSharp.Data;
using PhotoSharp.Models;
using System.Net.Mail;
using System.Security.Claims;

namespace Photosharp.Pages
{
    public class OrderModel : PageModel
    {
        private PhotoSharpDbContext db;
        public OrderModel(PhotoSharpDbContext db) => this.db = db;
        
        [BindProperty(SupportsGet =true)]
        public int Id { get; set; }
        public string UserId { get; set; }
        public Product Product { get; set;}
        public Order Order { get; set;}
        [BindProperty, EmailAddress, Required, Display(Name="Your Email Address")]
        public string OrderEmail { get; set; }
        [BindProperty, Required(ErrorMessage="Please supply a shipping address"), Display(Name="Shipping Address")]
        public string OrderShipping { get; set; } 
        [BindProperty, Display(Name="Quantity")]
        public int OrderQuantity { get; set; } = 1;
        [BindProperty]
        public Comment Comment { get; set; }  
        public List<Comment> CommentList2 { get; set; } = new List<Comment>();
        public async Task<IActionResult> OnPostAsync()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToPage("/Login");
            }
            Product = await db.Products.FindAsync(Id);
            if(ModelState.IsValid){
                var body = $@"<p>Thank you for Shopping from Us, we have received your order for {OrderQuantity} unit(s) of {Product.ProductName}!</p>
                <p>Your address is: <br/>{OrderShipping.Replace("\n", "<br/>")}</p>
                The total payment is ${Product.Price * OrderQuantity}.<br/>
                You will receive your order in 3 business Day .  Thanks!<br/>";
                using(var smtp = new SmtpClient())
                {
                    smtp.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                    smtp.PickupDirectoryLocation = @"/Users/My Files/Programming/John Abbott/15-ASP/PhotoSharp/wwwroot/CustomerEmail";
                    var message = new MailMessage();
                    message.To.Add(OrderEmail);
                    message.Subject = "PhotoSharp - New Message";
                    message.Body = body;
                    message.IsBodyHtml = true;
                    message.From = new MailAddress("Moha@idpd24.ca");
                    await smtp.SendMailAsync(message);
                }
                Order = await db.Orders.FindAsync(Id);
                UserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                var newOrder = new PhotoSharp.Models.Order { Quantity = OrderQuantity , OrderDate = DateTime.Now, Total = Product.Price * OrderQuantity , UserFK = UserId, ProductFK = Id};                
                db.Add(newOrder);
                Product.AvailableQuantity = Product.AvailableQuantity - OrderQuantity;
                await db.SaveChangesAsync();
                return RedirectToPage("OrderSuccess");
            }
            return Page();
        }
        public async Task<IActionResult> OnGetAsync(int? id)
        {
        Product = await db.Products.FindAsync(Id);
        var data2 = db.Comments.OrderByDescending(c => c.CommentId).Take(2).ToList();
                CommentList2 = data2;
        if (id == null)
        {
        return RedirectToPage("OrderSuccess");
        }
        Product = await db.Products.FirstOrDefaultAsync(m => m.Id == id);
        if (Product == null)
        {
        return RedirectToPage("PageNotFound");
        }
        return Page();
        }
    }
}
