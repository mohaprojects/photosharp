using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Photosharp.Pages;
using PhotoSharp.Models;
using PhotoSharp.Data;

namespace PhotoSharp.Pages
{
    public class LoginModel : PageModel
    {
        private PhotoSharpDbContext db;
        private readonly SignInManager<User> signInManager;
        private readonly ILogger<LoginModel> logger;
        public LoginModel(SignInManager<User> signInManager, ILogger<LoginModel> logger,PhotoSharpDbContext db)
        {
            this.signInManager = signInManager;
            this.logger = logger;
            this.db = db;
        }
        [BindProperty]
        public Comment Comment { get; set; }
        [BindProperty]
       public List<Comment> CommentList2 { get; set; } = new List<Comment>();
         [BindProperty]
        public InputModel Input { get; set; }
        public class InputModel
        {
            [Required]
            [EmailAddress]
            public string Email { get; set; }
            [Required]
            [DataType(DataType.Password)]
            public string Password { get; set; }
        }
        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid)
            {
                var result = await signInManager.PasswordSignInAsync(Input.Email, Input.Password, false, true);
                if (result.Succeeded)
                {
                    logger.LogInformation($"User {Input.Email} logged in");
                    return RedirectToPage("LoginSuccess");
                }
                else
                { 
                    ModelState.AddModelError(string.Empty, "Login failed (user does not exist, password invalid, or account locked out)");
                }
            }
            return Page();
        }
        public void OnGet()
        {
            var data2 = db.Comments.OrderByDescending(c => c.CommentId).Take(2).ToList();
                CommentList2 = data2;
        }
    }
}
