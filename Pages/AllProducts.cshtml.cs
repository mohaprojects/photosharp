using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PhotoSharp.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PhotoSharp.Models;

namespace PhotoSharp.Pages
{
    public class AllProductsModel : PageModel
    {
        private readonly PhotoSharpDbContext db;  
        public AllProductsModel(PhotoSharpDbContext db) => this.db = db;
        public List<Product> Products { get; set; } = new List<Product>();  
        [BindProperty]
        public Comment Comment { get; set; }  
        public List<Comment> CommentList2 { get; set; } = new List<Comment>();
        [BindProperty]
        public List<Product> data { get; set; } = new List<Product>();
        public async Task OnGetAsync()
        {
            Products = await db.Products.OrderByDescending(c => c.Id).ToListAsync();
             var data2 = db.Comments.OrderByDescending(c => c.CommentId).Take(2).ToList();
             CommentList2 = data2;          
        }
    }
}
