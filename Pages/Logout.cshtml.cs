using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using PhotoSharp.Models;
using PhotoSharp.Data;

namespace Photosharp.Pages
{
    public class LogoutModel : PageModel
    {
         private PhotoSharpDbContext db;
         private readonly SignInManager<User> signInManager;
        private readonly ILogger<LogoutModel> logger;
        public LogoutModel(SignInManager<User> signInManager, ILogger<LogoutModel> logger,PhotoSharpDbContext db)
        {
            this.signInManager = signInManager;
            this.logger = logger;
             this.db = db;
        }
      [BindProperty]
        public Comment Comment { get; set; }
        [BindProperty]
       public List<Comment> CommentList2 { get; set; } = new List<Comment>();
         public async Task<IActionResult> OnGetAsync()
        {
            var data2 = db.Comments.OrderByDescending(c => c.CommentId).Take(2).ToList();
                CommentList2 = data2;
            if (signInManager.IsSignedIn(User)) {
                logger.LogInformation($"User {User.Identity.Name} logged out");
            }
            await signInManager.SignOutAsync();
            return Page();
        }
    }
}
