using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PhotoSharp.Models;
using PhotoSharp.Data;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using System.IO;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using System.Text.RegularExpressions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;

namespace Photosharp.Pages
{
    [Authorize(Roles = "Admin")]
    public class AdminIndexModel : PageModel
    {

        public List<Order> OrderList { get; set; } = new List<Order>();
        public List<User> UserList { get; set; } = new List<User>();
        public List<Product> ProductList { get; set; } = new List<Product>();
        public List<Product> ProductNameList { get; set; } = new List<Product>();
        private PhotoSharpDbContext db;
        private readonly ILogger<RegisterModel> logger;
        private IHostEnvironment _environment;
        public AdminIndexModel(PhotoSharpDbContext db, ILogger<RegisterModel> logger, IHostEnvironment environment)
        {
            this.db = db;
            this.logger = logger;
            _environment = environment;
        }

        [BindProperty]
        public Order Order { get; set; }

        [BindProperty]
        public Product Product { get; set; }
        [BindProperty]
        public int ProductFK { get; set; }
        [BindProperty]
        public User user { get; set; }
        [BindProperty]
        public Comment Comment { get; set; }
        [BindProperty]
        public List<Comment> CommentList2 { get; set; } = new List<Comment>();
        [BindProperty]
        public string ProductName { get; set; }
        public Category Category { get; set; }
        [ForeignKey("Category")]
        public int CategoryFK { get; set; }

        [BindProperty]
        public string ProductDescription { get; set; }
        public string ProductImage1 { get; set; }
        public string ProductImage2 { get; set; }
        public string ProductImage3 { get; set; }
        public decimal Price { get; set; }
        public int AvailableQuantity { get; set; }
        public async Task OnGetAsync()
        {
            UserList = await db.Users.ToListAsync();
            ProductList = await db.Products.ToListAsync();
            OrderList = await db.Orders.ToListAsync();
            var data2 = db.Comments.OrderByDescending(c => c.CommentId).Take(2).ToList();
            CommentList2 = data2;
        }
    }
}
