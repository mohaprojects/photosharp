using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PhotoSharp.Data;
using PhotoSharp.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace Photosharp.Pages
{
    public class SearchTestModel : PageModel
    {
        private PhotoSharpDbContext db;
        public SearchTestModel(PhotoSharpDbContext db)
        {
            this.db = db;
        }
        [BindProperty]
        public InputModel Input { get; set; }
        public class InputModel
        {
            [BindProperty]
            public string Search { get; set; }
        }
        public List<Product> Products = new List<Product>();
        [BindProperty]
        public Comment Comment { get; set; }
        [BindProperty]
        public List<Comment> CommentList2 { get; set; } = new List<Comment>();
        public void OnGet()
        {
            var data2 = db.Comments.OrderByDescending(c => c.CommentId).Take(2).ToList();
            CommentList2 = data2;
        }
        public void OnPost()
        {
            var data2 = db.Comments.OrderByDescending(c => c.CommentId).Take(2).ToList();
            CommentList2 = data2;
            var res = db.Products.Where(i => i.ProductName.Contains(Input.Search)).ToList();
            Products = res;
        }
    }
}
