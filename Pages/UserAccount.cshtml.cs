using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using PhotoSharp.Models;
using PhotoSharp.Data;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;

namespace Photosharp.Pages
{
    public class UserAccountModel : PageModel
    {
        private PhotoSharpDbContext db;
        private readonly UserManager<User> userManager;
        private readonly ILogger<RegisterModel> logger;
        public User CurrUser { get; set; }

        [BindProperty]
        public InputModel Input { get; set; }

        public UserAccountModel(UserManager<User> userManager,
              ILogger<RegisterModel> logger, PhotoSharpDbContext db)
        {
            this.userManager = userManager;
            this.logger = logger;
            this.db = db;
        }
        [BindProperty]
        public Comment Comment { get; set; }
        [BindProperty]
        public List<Comment> CommentList2 { get; set; } = new List<Comment>();
        public string UserId { get; set; }
        public List<Order> OrderList { get; set; } = new List<Order>();
        public class InputModel
        {
            public string FullName { get; set; }
            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }
            public string Address { get; set; }
            public string PostalCode { get; set; }
        }
        public async Task<IActionResult> OnGetAsync()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToPage("/Login");
            }
            UserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var data2 = db.Comments.OrderByDescending(c => c.CommentId).Take(2).ToList();
            CommentList2 = data2;
            var usr = (from u in db.Users
                       where u.Id == UserId
                       select u).SingleOrDefault();
            CurrUser = usr;
            OrderList = await db.Orders
               .Where(o => o.UserFK == UserId)
               .Include(o => o.Product)
               .ToListAsync();
            return Page();
        }
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var UserFromDB = db.Users.FirstOrDefault(x => x.Id == userId);
            UserFromDB.FullName = Input.FullName;
            UserFromDB.Address = Input.Address;
            UserFromDB.PostalCode = Input.PostalCode;
            UserFromDB.Email = Input.Email;
            UserFromDB.PasswordHash = Input.Password;
            await db.SaveChangesAsync();
            return RedirectToPage("/index");
        }
    }
}
