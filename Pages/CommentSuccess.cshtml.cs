using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PhotoSharp.Data;
using PhotoSharp.Models;

namespace PhotoSharp.Pages
{
    public class CommentSuccessModel : PageModel
    {
        private PhotoSharpDbContext db;
        public CommentSuccessModel(PhotoSharpDbContext db)
        {
            this.db = db;
        }
        [BindProperty]
        public Comment Comment { get; set; }
        [BindProperty]
        public List<Comment> CommentList2 { get; set; } = new List<Comment>();

        [BindProperty(SupportsGet = true)]
        public string Email { get; set; }
        public void OnGet()
        {
             var data2 = db.Comments.OrderByDescending(c => c.CommentId).Take(2).ToList();

            CommentList2 = data2;
        }
    }
}
