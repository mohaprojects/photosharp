using System.Linq;
using System.Threading.Tasks;
using PhotoSharp.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PhotoSharp.Models;
using System.Collections.Generic;

namespace PhotoSharp.Pages
{
    public class CategoriesModel : PageModel
    {
        private readonly PhotoSharpDbContext db;
        public CategoriesModel(PhotoSharpDbContext db) => this.db = db;
        public List<Product> Products { get; set; } = new List<Product>();
        [BindProperty]
        public Comment Comment { get; set; }
        public List<Comment> CommentList2 { get; set; } = new List<Comment>();
        public Category Category { get; set; }
        public Product Product { get; set; }
        public List<Product> ProductList = new List<Product>();
        public void OnGet(int? Id)
        {
            Products = db.Products.ToList();
            var data = (from cat in db.Categories
                        where cat.CategoryId == Id
                        select cat).FirstOrDefault();
            Category = data;           
            foreach (var cat in Products)
            {
                if (cat.CategoryFK == Id)
                {
                    ProductList.Add(cat);
                }
            }
            var data2 = db.Comments.OrderByDescending(c => c.CommentId).Take(2).ToList();
            CommentList2 = data2;
        }
    }
}
