using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PhotoSharp.Models;
using PhotoSharp.Data;
namespace Photosharp.Pages
{
    public class AdminDeleteModel : PageModel
    {
        private PhotoSharpDbContext db;
        [BindProperty]
        public Comment Comment { get; set; }
        [BindProperty]
        public List<Comment> CommentList2 { get; set; } = new List<Comment>();
        public AdminDeleteModel(PhotoSharpDbContext db)
        {
            this.db = db;
        }
        [BindProperty]
        public Product Product { get; set; }
        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            Product = await db.Products.FindAsync(id);
            if (Product != null)
            {
                db.Products.Remove(Product);
                await db.SaveChangesAsync();
            }
            return RedirectToPage("./AdminIndex");
        }
        public void OnGet()
        {
            var data2 = db.Comments.OrderByDescending(c => c.CommentId).Take(2).ToList();

            CommentList2 = data2;
        }
    }
}
