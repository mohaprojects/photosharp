using System;
using System.Net.Mail;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PhotoSharp.Models;
using PhotoSharp.Data;
using System.Net;

namespace PhotoSharp.Pages
{
    public class AboutModel : PageModel
    {
        private PhotoSharpDbContext db;  
        public AboutModel(PhotoSharpDbContext db) => this.db = db;
        [BindProperty]
        public string FName { get; set; } 
        [BindProperty]
        public string LName { get; set; }
        [BindProperty]
        public string Message { get; set; } 
        [BindProperty]
        public string CEmail { get; set; }

        [BindProperty]
        public Comment Comment { get; set; }
        public List<Comment> CommentList2 { get; set; } = new List<Comment>();          
        public async Task<IActionResult> OnPostAsync()
        {
            if(ModelState.IsValid){
            
            var body = $@"<h5>Hello {FName} {LName},</h5> 
                          <p>We have received your comment: <h5 style= 'color: blue'>{Message}</h5> </p>
                          <p>Your Email address is: <br/>{CEmail.Replace("\n", "<br/>")}</p>
                             We will contact you soon.  Thanks!<br/>";
            using(var smtp = new SmtpClient())
            {
                smtp.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                smtp.PickupDirectoryLocation = @"/Users/My Files/Programming/John Abbott/15-ASP/PhotoSharp/wwwroot/CustomerEmail";
                var message = new MailMessage();
                message.To.Add(CEmail);
                message.Subject = "PhotoSharp - New Message";
                message.Body = body;
                message.IsBodyHtml = true;
                message.From = new MailAddress("photosharp@mail.com");
                await smtp.SendMailAsync(message);
            }
            return RedirectToPage("Index");
        }
        return Page();
        }
        public void OnGet()
        {
            var data2 = db.Comments.OrderByDescending(c => c.CommentId).Take(2).ToList();
                CommentList2 = data2;
        }
    }
}
