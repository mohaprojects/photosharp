using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace PhotoSharp.Models
{
    public class Comment
    {
        public int CommentId { get; set; }

        public User User { get; set; }
        [ForeignKey("User")]
        public string UserFK { get; set; }
        public Product Product { get; set; }
        [ForeignKey("Product")]
        public int ProductFK { get; set; }
        public string CommentBody { get; set; }
    }
}