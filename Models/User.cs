using Microsoft.AspNetCore.Identity;

namespace PhotoSharp.Models
{
    public class User : IdentityUser
    {
        public string FullName { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
    }
}