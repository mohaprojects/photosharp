using System;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace PhotoSharp.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        public Product Product { get; set; }
        [ForeignKey("Product")]
        public int ProductFK { get; set; }
        public User User { get; set; }
        [ForeignKey("User")]
        public string UserFK { get; set; }
        public int Quantity { get; set; }
        public DateTime OrderDate { get; set; }
        public decimal Total { get; set; }
    }
}