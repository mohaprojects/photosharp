using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace PhotoSharp.Models
{
    public class Product
    {
        // GENERAL
        public int Id { get; set; }
        public string ProductName { get; set; }
        public Category Category { get; set; }
        [ForeignKey("Category")]
        public int CategoryFK { get; set; }
        public string ProductDescription { get; set; } // Rich text / HTML (use textarea, ideally with JS rich text editor)
        public string ProductImage1 { get; set; }
        public string ProductImage2 { get; set; }
        public string ProductImage3 { get; set; }
        public decimal Price { get; set; }
        public int AvailableQuantity { get; set; }
    }
}