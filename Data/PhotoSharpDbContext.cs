using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PhotoSharp.Models;


namespace PhotoSharp.Data
{
    public class PhotoSharpDbContext : IdentityDbContext<User>
    {
        public PhotoSharpDbContext(DbContextOptions<PhotoSharpDbContext> options) : base(options) { }
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Order> Orders { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data source=PhotoSharp.sqlite");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // On event model creating
            base.OnModelCreating(modelBuilder);
        }
    }
}